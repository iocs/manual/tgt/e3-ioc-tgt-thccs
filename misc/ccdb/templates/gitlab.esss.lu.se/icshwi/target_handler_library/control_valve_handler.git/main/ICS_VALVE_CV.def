###################################### ICS HWI ###############################################
#############################      ICS Handler Library    ####################################
##                                           	                                            ## 
##                                                                                          ##  
##                                  CV - Control valve                                      ##
##                                                                                          ##  
##                                                                                          ##  
############################           Version: 1.0           ################################

############################
#  STATUS BLOCK
############################ 
define_status_block()

#Operation modes
add_digital("OpMode_Auto",      PV_DESC="Operation Mode Auto",        PV_ONAM="True",   PV_ZNAM="False",  ARCHIVE=True)      
add_digital("OpMode_Manual",    PV_DESC="Operation Mode Manual",      PV_ONAM="True",   PV_ZNAM="False",  ARCHIVE=True)
add_digital("OpMode_Forced",    PV_DESC="Operation Mode Forced",      PV_ONAM="True",   PV_ZNAM="False",  ARCHIVE=True)
add_analog("DeviceColor","INT", PV_DESC="BlockIcon valve color")


#Valve states
add_analog("ValvePosition","REAL",      PV_DESC="Valve Position AI",            PV_EGU="%", ARCHIVE=True)
add_analog("ValveSetpoint","REAL",       PV_DESC="Valve Setpoint",               PV_EGU="%", ARCHIVE=True)
add_analog("ValvePosition_Cmd","REAL",  PV_DESC="Valve Control AO",             PV_EGU="%", ARCHIVE=True)
add_digital("Opening",                  PV_DESC="Valve opening",                PV_ONAM="Opening",  PV_ZNAM="NotMoving",    ARCHIVE=True)
add_digital("Closing",                  PV_DESC="Valve closing",                PV_ONAM="Closing",          PV_ZNAM="NotMoving",    ARCHIVE=True)
add_digital("Opened",                   PV_DESC="Opened Limit Switch",          PV_ONAM="True",             PV_ZNAM="Opened",       ARCHIVE=True)
add_digital("Closed",                   PV_DESC="Closed Limit Switch",          PV_ONAM="True",             PV_ZNAM="Closed",       ARCHIVE=True)

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("InhibitManual",            PV_DESC="Inhibit Manual Mode",          PV_ONAM="InhibitManual",    PV_ZNAM="AllowManual")
add_digital("InhibitForce",             PV_DESC="Inhibit Force Mode",           PV_ONAM="InhibitForce",     PV_ZNAM="AllowForce")


#Interlock signals
add_digital("GroupInterlock",           PV_DESC="Group Interlock",      PV_ONAM="True",     PV_ZNAM="False")
add_digital("MoveInterlock",            PV_DESC="Move Interlock",       PV_ONAM="True",     PV_ZNAM="False",  ARCHIVE=True)
add_string("InterlockMsg", 39,          PV_NAME="InterlockMsg",         PV_DESC="Interlock Message")

#for OPI visualization
add_digital("EnableAutoBtn",                         PV_DESC="Enable Auto Button",         PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableManualBtn",                       PV_DESC="Enable Manual Button",       PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableForcedBtn",                       PV_DESC="Enable Force Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableFuse",	                       	 PV_DESC="Enable Fuse Visibility",     PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableActuatorFault",                   PV_DESC="Enable Actuator Fault Vis",  PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableActuatorRemote",                  PV_DESC="Enable Actuator Remote Vis", PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableForceValBtn",                     PV_DESC="Enable Force Value Button",  PV_ONAM="True",             PV_ZNAM="False")
add_digital("AlarmLatching",                         PV_DESC="Latching of the alarms")
add_digital("GroupAlarm",                            PV_DESC="Group Alarm for OPI")

#Alarm signals
add_major_alarm("DiscrepancyAlarm",     "Position Discrepancy",         PV_DESC="Position Discrepancy", PV_ONAM="Position Discrepancy",     PV_ZNAM="False")
add_major_alarm("IO_Error",             "HW IO Error",                  PV_DESC="HW IO Error",          PV_ONAM="HW IO Error",              PV_ZNAM="False")
add_minor_alarm("SPLimitActive",        "SPLimitActive",                PV_DESC="SPLimitActive",        PV_ONAM="SPLimitActive",            PV_ZNAM="False")
add_major_alarm("SSTriggered",          "SSTriggered",                  PV_DESC="SSTriggered",          PV_ONAM="SSTriggered",              PV_ZNAM="False")
add_major_alarm("FuseTripped",          "Fuse Tripped",                 PV_DESC="Fuse Tripped",         PV_ONAM="Fuse Tripped",             PV_ZNAM="False")
add_major_alarm("ActuatorFault",        "Actuator Fault",               PV_DESC="Actuator Fault",       PV_ONAM="Actuator Fault",           PV_ZNAM="False")
add_minor_alarm("ActuatorRemote",       "Not in remote mode",           PV_DESC="Not in remote mode",   PV_ONAM="Not in remote mode",       PV_ZNAM="False")

#Discrepancy
add_analog("DiscrPerc","REAL" ,     PV_DESC="Discrepancy In Percent",       PV_EGU="%")
add_time("DiscrTime",               PV_DESC="Discrepancy Time Interval")

#Ramping
add_analog("MaxRampUpSpeed","REAL",     PV_DESC="Maximum Ramping UP Speed",     PV_EGU="%/s")
add_analog("MaxRampDownSpeed","REAL",   PV_DESC="Maximum Ramping DOWN Speed",   PV_EGU="%/s")
add_analog("ActRampSpeed","REAL",       PV_DESC="Actual Ramping Speed",         PV_EGU="%/s")
add_analog("FB_RampUpTime","INT",       PV_DESC="Ramping UP time",              PV_EGU="sec")
add_analog("FB_RampUpRange","REAL",     PV_DESC="Ramping UP range",             PV_EGU="%")
add_analog("FB_RampDownTime","INT",     PV_DESC="Ramping DOWN time",            PV_EGU="sec")
add_analog("FB_RampDownRange","REAL",   PV_DESC="Ramping DOWN range",           PV_EGU="%")
add_digital("Ramping",                  PV_DESC="Ramping Indicator",            PV_ONAM="True",     PV_ZNAM="False")
add_digital("RampSettingOK",            PV_DESC="Ramping can be enabled",       PV_ONAM="True",     PV_ZNAM="False")

#Feedbacks
add_analog("FB_ForcePosInput","REAL" ,  PV_DESC="FB Forced input position",                 PV_EGU="%")
add_analog("FB_ForcePosOutput","REAL" , PV_DESC="FB Forced output position",                PV_EGU="%")
add_analog("FB_Setpoint","REAL" ,       PV_DESC="FB Setpoint from HMI (SP)",                PV_EGU="%")
add_analog("FB_Step","REAL" ,           PV_DESC="FB Step value for Open/Close",             PV_EGU="%")
add_analog("FB_RampTime","INT" ,        PV_DESC="Ramping Time",                             PV_EGU="sec")
add_analog("FB_RampRange","REAL" ,      PV_DESC="Ramping Range",                            PV_EGU="%")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",         PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",       PV_DESC="CMD: Manual Mode")
add_digital("Cmd_Force",        PV_DESC="CMD: Force Mode")
add_digital("Cmd_RampON",       PV_DESC="Turn Ramping ON")
add_digital("Cmd_RampOFF",      PV_DESC="Turn Ramping OFF")
add_digital("Cmd_AckAlarm",     PV_DESC="CMD: Acknowledge Alarm")
add_digital("Cmd_ForceValInp",  PV_DESC="CMD: Force Valve PLC Input")
add_digital("Cmd_ForceValOut",  PV_DESC="CMD: Force Valve PLC Output")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Setpoint and Force mode value from HMI
add_analog("P_ForcePosInput","REAL" ,               PV_DESC="Forced input position",                   PV_EGU="%")
add_analog("P_ForcePosOutput","REAL" ,              PV_DESC="Forced output position",                  PV_EGU="%")
add_analog("P_Setpoint","REAL" ,                    PV_DESC="Setpoint from HMI (SP)",                  PV_EGU="%")

#Step value
add_analog("P_Step","REAL" ,                         PV_DESC="Step value for open close",               PV_EGU="%")

#Ramping speed from the HMI
add_analog("P_RampUpTime","INT",                     PV_DESC="Ramping UP Time",                         PV_EGU="sec")
add_analog("P_RampUpRange","REAL",                   PV_DESC="Ramping UP Range",                        PV_EGU="%")
add_analog("P_RampDownTime","INT",                   PV_DESC="Ramping DOWN Time",                       PV_EGU="sec")
add_analog("P_RampDownRange","REAL",                 PV_DESC="Ramping DOWN Range",                      PV_EGU="%")
